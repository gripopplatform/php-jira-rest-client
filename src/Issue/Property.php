<?php

namespace JiraRestApi\Issue;

class Property implements \JsonSerializable
{
    /** @var string */
    public $key;

    /** @var mixed */
    public $value;

    public function __construct($key, $value = null)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function jsonSerialize()
    {
        return array_filter(get_object_vars($this));
    }
}
