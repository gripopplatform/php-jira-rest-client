<?php

namespace JiraRestApi\Issue;

class CommentV3 extends Comment
{
    /** @var \JiraRestApi\Issue\DescriptionV3|null */
    public $body;
    /** @var \JiraRestApi\Issue\Property[]|null */
    public $properties;

    public function setBody($body)
    {
        return $this->addCommentParagraph($body);
    }

    /**
     * Set a comment property.
     *
     * @param Property $property
     *
     * @return $this
     *
     */
    public function setProperty($property)
    {
        if (is_null($this->properties)) {
            $this->properties = [];
        }
        else {
            $this->removeProperty($property);
        }

        $this->properties[] = $property;

        return $this;
    }

    /**
     * Set a comment property.
     *
     * @param Property $property
     *
     * @return $this
     *
     */
    public function removeProperty($property)
    {
        if (!is_null($this->properties)) {
            $this->properties = array_filter($this->properties, function($item) use($property) { return $item->getKey() !== $property->getKey(); });
        }

        return $this;
    }

    /**
     * Set the service desk 'public' attribute via V3 api.
     *
     * @param bool $public
     *
     * @return $this
     *
     */
    public function setPublic($public = true)
    {
        if (!$public) {
            $this->setProperty(new Property('sd.public.comment', ['internal' => !$public]));
        }
        else {
            $this->removeProperty(new Property('sd.public.comment'));
        }

        return $this;
    }

    /**
     * @param \JiraRestApi\Issue\DescriptionV3|null $description
     *
     * @return $this
     */
    public function addCommentParagraph($description)
    {
        if (empty($this->body)) {
            $this->body = new DescriptionV3();
        }

        $this->body->addDescriptionContent('paragraph', $description);

        return $this;
    }

    /**
     * @return $this
     */
    public function addCommentNode($type, $text = null, $attrs = [])
    {
        if (empty($this->body)) {
            $this->body = new DescriptionV3();
        }

        $this->body->addDescriptionContent($type, $text, $attrs);

        return $this;
    }

    /**
     * @param \JiraRestApi\Issue\Attachment[] $attachments
     *
     * @return $this
     *
     * Attachments must have the mediaId attribute set. At the moment the only way to
     * achieve this is by explicitly retrieving them via AttachmentService::get.
     */
    public function addCommentAttachments($attachments)
    {
        if (empty($this->body)) {
            $this->body = new DescriptionV3();
        }

        $this->body->addDescriptionAttachments($attachments);

        return $this;
    }
}
