<?php

namespace JiraRestApi\Configuration;

/**
 * Class AbstractConfiguration.
 */
abstract class AbstractConfiguration implements ConfigurationInterface
{
    /**
     * Jira host.
     *
     * @var string
     */
    protected $jiraHost;

    /**
     * Jira login.
     *
     * @var string
     */
    protected $jiraUser;

    /**
     * Jira password.
     *
     * @var string
     */
    protected $jiraPassword;

    /**
     * Enabled write to log.
     *
     * @var bool
     */
    protected $jiraLogEnabled;

    /**
     * Path to log file.
     *
     * @var string
     */
    protected $jiraLogFile;

    /**
     * Log level (DEBUG, INFO, ERROR, WARNING).
     *
     * @var string
     */
    protected $jiraLogLevel;

    /**
     * Curl options CURLOPT_SSL_VERIFYHOST.
     *
     * @var bool
     */
    protected $curlOptSslVerifyHost;

    /**
     * Curl options CURLOPT_SSL_VERIFYPEER.
     *
     * @var bool
     */
    protected $curlOptSslVerifyPeer;

    /**
     * Curl option CURLOPT_USERAGENT.
     *
     * @var string
     */
    protected $curlOptUserAgent;

    /**
     * Curl options CURLOPT_VERBOSE.
     *
     * @var bool
     */
    protected $curlOptVerbose;

    /**
     * HTTP header 'Authorization: Bearer {token}' for OAuth.
     *
     * @var string
     */
    protected $oauthAccessToken;

    /**
     * enable cookie authorization.
     *
     * @var bool
     */
    protected $cookieAuthEnabled;

    /**
     * HTTP cookie file name.
     *
     * @var string
     */
    protected $cookieFile;

    /**
     * Proxy server.
     *
     * @var string
     */
    protected $proxyServer;

    /**
     * Proxy port.
     *
     * @var string
     */
    protected $proxyPort;

    /**
     * Proxy user.
     *
     * @var string
     */
    protected $proxyUser;

    /**
     * Proxy password.
     *
     * @var string
     */
    protected $proxyPassword;

    /**
     * Use Jira Cloud REST API v3.
     *
     * @var bool
     */
    protected $useV3RestApi;

    /** @var string */
    protected $curlOptSslCert;

    /** @var string */
    protected $curlOptSslCertPassword;

    /** @var string */
    protected $curlOptSslKey;

    /** @var string */
    protected $curlOptSslKeyPassword;

    /** @var int */
    protected $timeout = 60;

    /** @var bool */
    protected $useTokenBasedAuth;

    /** @var string|null */
    protected $personalAccessToken;

    /** @var int|null */
    protected $serviceDeskId;

    public function getJiraHost(): string
    {
        return $this->jiraHost;
    }

    public function getJiraUser(): string
    {
        return $this->jiraUser;
    }

    public function getJiraPassword(): string
    {
        return $this->jiraPassword;
    }

    public function getJiraLogEnabled(): bool
    {
        return $this->jiraLogEnabled;
    }

    public function getJiraLogFile(): string
    {
        return $this->jiraLogFile;
    }

    public function getJiraLogLevel(): string
    {
        return $this->jiraLogLevel;
    }

    public function isCurlOptSslVerifyHost(): bool
    {
        return $this->curlOptSslVerifyHost;
    }

    public function isCurlOptSslVerifyPeer(): bool
    {
        return $this->curlOptSslVerifyPeer;
    }

    public function isCurlOptSslCert(): ?string
    {
        return $this->curlOptSslCert;
    }

    public function isCurlOptSslCertPassword(): ?string
    {
        return $this->curlOptSslCertPassword;
    }

    public function isCurlOptSslKey(): ?string
    {
        return $this->curlOptSslKey;
    }

    public function isCurlOptSslKeyPassword(): ?string
    {
        return $this->curlOptSslKeyPassword;
    }

    public function isCurlOptVerbose(): bool
    {
        return $this->curlOptVerbose;
    }

    /**
     * Get curl option CURLOPT_USERAGENT.
     */
    public function getCurlOptUserAgent(): ?string
    {
        return $this->curlOptUserAgent;
    }

    public function getOAuthAccessToken(): string
    {
        return $this->oauthAccessToken;
    }

    public function isCookieAuthorizationEnabled(): bool
    {
        return $this->cookieAuthEnabled;
    }

    /**
     * get default User-Agent String.
     */
    public function getDefaultUserAgentString(): string
    {
        $curlVersion = curl_version();

        return sprintf('curl/%s (%s)', $curlVersion['version'], $curlVersion['host']);
    }

    public function getCookieFile(): ?string
    {
        return $this->cookieFile;
    }

    public function getProxyServer(): ?string
    {
        return $this->proxyServer;
    }

    public function getProxyPort(): ?string
    {
        return $this->proxyPort;
    }

    public function getProxyUser(): ?string
    {
        return $this->proxyUser;
    }

    public function getProxyPassword(): ?string
    {
        return $this->proxyPassword;
    }

    public function getUseV3RestApi(): bool
    {
        return $this->useV3RestApi;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function isTokenBasedAuth(): bool
    {
        return $this->useTokenBasedAuth;
    }

    public function getPersonalAccessToken(): string
    {
        return $this->personalAccessToken;
    }

    public function getServiceDeskId(): int
    {
        return $this->serviceDeskId;
    }
}
